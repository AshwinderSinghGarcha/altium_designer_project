Project Outputs Generation Report
---------------------------------
Start Output Generation At 9:15:42 PM On 2018-07-10

Output: Gerber Files
Type  : Gerber
From  : Variant [[No Variations]] of PCB Document [28Pins_v111_pcb.PcbDoc]
   Generated File[28Pins_v111_pcb.GTL]
   Generated File[28Pins_v111_pcb.GBL]
   Generated File[28Pins_v111_pcb.GTO]
   Generated File[28Pins_v111_pcb.GTP]
   Generated File[28Pins_v111_pcb.GTS]
   Generated File[28Pins_v111_pcb.GBS]
   Generated File[28Pins_v111_pcb.GBO]
   Generated File[28Pins_v111_pcb.GM2]
   Generated File[28Pins_v111_pcb.GM28]
   Generated File[28Pins_v111_pcb.RUL]
   Generated File[28Pins_v111_pcb.EXTREP]
   Generated File[28Pins_v111_pcb.REP]


Files Generated   : 12
Documents Printed : 0

Finished Output Generation At 9:15:47 PM On 2018-07-10
